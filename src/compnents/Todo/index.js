import React, { useState } from "react";
import List from "./List";

function Todo() {

  const [taskname, setTaskname] = useState("");
  const [lists, setLists] = useState([]);
  
  const addList = () => {
    if(taskname !== ''){
      console.log(taskname);
      let listItem = {
        "id": Date.now(),
          "name": taskname,
          "state": 'active'
      }

      setLists([...lists, listItem]);
      setTaskname("");
    }
  }

      

  
  const removeItem = (checklist, id) => {
    if(checklist.filter((list) => list === true).length > 0){
      const checked = checklist.map((list, index) => list === true ? index : '');
      console.log(checked);
      const presentList = lists.filter((i, index) => checked.indexOf(index) == -1);
      console.log(presentList);
      setLists([...presentList]);  
    }else{
      console.log(id);
        const presentList = lists.filter((i) => i.id != id);
        setLists([...presentList]);  
    }
    
  }

  const updateItem = (index) => {
    const newlists = [...lists];
    console.log(newlists);
    const item = newlists[index];
    let newItem = prompt(`Update ${item.name}?`, item.name);
    let todoObj = { name: newItem};
    newlists.splice(index, 1, todoObj);
    if (newItem === null || newItem === "") {
      return;
    } else {
      item.todo = newItem;
    }
    setLists(newlists);
  };


  return (
    <div className='container text-center'>
      <h1>TO DO LIST!</h1>
      <div className="task-form">
            <input type="text" placeholder="New Task" value={taskname} onChange={(event) => setTaskname(event.target.value)} className="form-control" />
            <div>&nbsp;</div>
            <button type="button" onClick={addList} className="btn btn-primary">Add</button>
      </div>
      <div className="task-list">
            <List items={lists} removeItem={(checklist, id) => removeItem(checklist, id)} updateItem={(id) => {updateItem(id)}}/>
      </div>
    </div>
  );
}

export default Todo;
