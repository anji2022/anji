import React from "react";
import Todo from "../Todo";

function index() {
  return (
    <div className="container text-center">
      <h1>Welcome The Dashboad!</h1>
        <div className="task-container text-left">
              <Todo />
      </div>
    </div>
    
  );
}

export default index;
